-- [1]

SELECT customerName FROM customers WHERE country = "Philippines";

-- [2]

SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";

-- [3]

SELECT productName, MSRP FROM products WHERE productName = "The Titanic";

-- [4]

SELECT firstName, lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";

-- [5]

SELECT customerName FROM customers WHERE state IS null;

-- [6]

SELECT firstName, lastName, email FROM employees WHERE lastName = "Patterson" AND firstName = "Steve";

-- [7]

SELECT customerName, country, creditLimit FROM customers WHERE creditLimit > 3000;

-- [8]

SELECT orderNumber FROM orders WHERE comments LIKE "%DHL%";

-- [9]

SELECT * FROM productlines WHERE textDescription LIKE "%state of the art%";

-- [10]

SELECT DISTINCT country FROM customers;

-- [11]

SELECT DISTINCT status FROM orders;

-- [12]

SELECT customerName, country FROM customers WHERE country = "USA" OR country = "France" OR country = "Canada";

-- [13]

SELECT firstName, lastName, officeCode FROM employees WHERE officeCode = 5;

-- [14]

SELECT customerName FROM customers WHERE salesRepEmployeeNumber = 1166;

-- [15]

SELECT productName, customerName FROM customers
	JOIN orders ON customers.customerNumber = orders.customerNumber
	JOIN orderdetails ON orders.orderNumber = orderdetails.orderNumber
	JOIN products ON orderdetails.productCode = products.productCode;

-- [16]

SELECT 
    employees.firstName, 
    employees.lastName, 
    customers.customerName, 
    offices.country 
FROM 
    employees 
    INNER JOIN offices ON employees.officeCode = offices.officeCode 
    INNER JOIN customers ON employees.employeeNumber = customers.salesRepEmployeeNumber
WHERE 
    offices.country = customers.country;

-- [17]

SELECT productName, quantityInStock FROM products WHERE productLine = "Planes" AND quantityInStock <1000;

-- [18]

SELECT customerName FROM customers WHERE phone LIKE "%+81%";